import 'package:flutter/material.dart';
import 'package:flutter_login_app/account.dart';
import 'package:flutter_login_app/camera.dart';
import 'package:flutter_login_app/home.dart';
import 'package:flutter_login_app/notification.dart';
import 'package:flutter_login_app/setting.dart';

class NavPage extends StatefulWidget {
  const NavPage({super.key});

  @override
  State<NavPage> createState() => _NavPageState();
}

class _NavPageState extends State<NavPage> {
  int selectedPage = 0;
  final _pageOptions = [
    HomePage(),
    NotifPage(),
    CameraPage(),
    AccountPage(),
    SettingPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pageOptions[selectedPage],
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          border: Border(
            top: BorderSide(
              width: 1.0,
            ),
          ),
        ),
        child: BottomNavigationBar(
          currentIndex: selectedPage,
          onTap: (Index) {
            setState(() {
              selectedPage = Index;
            });
          },
          selectedItemColor: Colors.blue,
          unselectedItemColor: Colors.grey,
          backgroundColor: Colors.grey[800],
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.notifications),
              label: 'Notification',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.camera_alt),
              label: 'Camera',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'Profile',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              label: 'Settings',
            ),
          ],
        ),
      ),
    );
  }
}
