import 'package:flutter/material.dart';
import 'package:flutter_login_app/notification.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
int Index = 0;

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double containerWidth = screenWidth * 0.7;
     Widget child;

    return Scaffold(
      backgroundColor: Colors.greenAccent,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Icon(
              Icons.star,
              color: Colors.yellowAccent,
              size: 25.0,
            ),
            Icon(
              Icons.diamond,
              color: Colors.blueAccent,
              size: 25.0,
            ),
            Stack(
              children: [
                Icon(
                  Icons.favorite,
                  color: Colors.redAccent,
                  size: 25.0,
                ),
                Positioned(
                  top: -4,
                  right: -4,
                  child: Container(
                    padding: EdgeInsets.all(1),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.circle,
                    ),
                    constraints: BoxConstraints(
                      minWidth: 16,
                      minHeight: 16,
                    ),
                    child: Center(
                      child: Text(
                        '3',
                        style: TextStyle(
                          color: Colors.redAccent,
                          fontSize: 10,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            width: containerWidth,
            child: Column(
              children: List.generate(20, (index) {
                MainAxisAlignment rowAlignment;
                if (index % 4 == 0) {
                  rowAlignment = MainAxisAlignment.center;
                } else if (index % 4 == 1) {
                  rowAlignment = MainAxisAlignment.start;
                } else if (index % 4 == 2) {
                  rowAlignment = MainAxisAlignment.center;
                } else {
                  rowAlignment = MainAxisAlignment.end;
                }
                return Container(
                  child: Row(
                    mainAxisAlignment: rowAlignment,
                    children: List.generate(1, (subIndex) {
                      return InkWell(
                        onTap: () {
                          // Handle button press
                        },
                        child: Container(
                          child: Row(
                            children: [
                              Column(
                                children: [
                                  Image.asset(
                                    'assets/images/current_level.png',
                                    width: 125.0,
                                    height: 125.0,
                                  ),
                                  Text(
                                    'Stage ${index + 1}',
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      );
                    }),
                  ),
                );
              }),
            ),
          ),
        ),
      ),
    );
  }
}
